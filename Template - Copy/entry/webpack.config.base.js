module.exports = {
    // devtool: 'inline-source-map',
    externals: {
        'Config': JSON.stringify(require('../config.json'))
    },
    module: {

        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader?cacheDirectory=true"
                }
            },
        ]
    },
};