/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"main": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push(["./src/index.js","vendor"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/KeyCodes.js":
/*!*************************!*\
  !*** ./src/KeyCodes.js ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n    value: true\n});\nvar KeyCodes = {\n    KeySpace: 32,\n    KeyA: 65,\n    KeyB: 66,\n    KeyC: 67,\n    KeyD: 68,\n    KeyE: 69,\n    KeyF: 70,\n    KeyG: 71,\n    KeyH: 72,\n    KeyI: 73,\n    KeyJ: 74,\n    KeyK: 75,\n    KeyL: 76,\n    KeyM: 77,\n    KeyN: 78,\n    KeyO: 79,\n    KeyP: 80,\n    KeyQ: 81,\n    KeyR: 82,\n    KeyS: 83,\n    KeyT: 84,\n    KeyU: 85,\n    KeyV: 86,\n    KeyW: 87,\n    KeyX: 88,\n    KeyY: 89,\n    KeyZ: 90,\n    KeyShiftA: 91,\n    KeyShiftB: 92,\n    KeyShiftC: 93,\n    KeyShiftD: 94,\n    KeyShiftE: 95,\n    KeyShiftF: 96,\n    KeyShiftG: 97,\n    KeyShiftH: 98,\n    KeyShiftI: 99,\n    KeyShiftJ: 100,\n    KeyShiftK: 101,\n    KeyShiftL: 102,\n    KeyShiftM: 103,\n    KeyShiftN: 104,\n    KeyShiftO: 105,\n    KeyShiftP: 106,\n    KeyShiftQ: 107,\n    KeyShiftR: 108,\n    KeyShiftS: 109,\n    KeyShiftT: 110,\n    KeyShiftU: 111,\n    KeyShiftV: 112,\n    KeyShiftW: 113,\n    KeyShiftX: 114,\n    KeyShiftY: 115,\n    KeyShiftZ: 116,\n    Key0: 48,\n    Key1: 49,\n    Key2: 50,\n    Key3: 51,\n    Key4: 52,\n    Key5: 53,\n    Key6: 54,\n    Key7: 55,\n    Key8: 56,\n    Key9: 57,\n    KeyArrowLeft: 37,\n    KeyArrowUp: 38,\n    KeyArrowRight: 39,\n    KeyArrowDown: 40,\n    KeyBackspace: 8\n\n};\n\nexports.default = KeyCodes;//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvS2V5Q29kZXMuanMuanMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vc3JjL0tleUNvZGVzLmpzPzE3ZDMiXSwic291cmNlc0NvbnRlbnQiOlsidmFyIEtleUNvZGVzID0ge1xyXG4gICAgS2V5U3BhY2U6IDMyLFxyXG4gICAgS2V5QSA6IDY1LFxyXG4gICAgS2V5QiA6IDY2LFxyXG4gICAgS2V5QyA6IDY3LFxyXG4gICAgS2V5RCA6IDY4LFxyXG4gICAgS2V5RSA6IDY5LFxyXG4gICAgS2V5RiA6IDcwLFxyXG4gICAgS2V5RyA6IDcxLFxyXG4gICAgS2V5SCA6IDcyLFxyXG4gICAgS2V5SSA6IDczLFxyXG4gICAgS2V5SiA6IDc0LFxyXG4gICAgS2V5SyA6IDc1LFxyXG4gICAgS2V5TCA6IDc2LFxyXG4gICAgS2V5TSA6IDc3LFxyXG4gICAgS2V5TiA6IDc4LFxyXG4gICAgS2V5TyA6IDc5LFxyXG4gICAgS2V5UCA6IDgwLFxyXG4gICAgS2V5USA6IDgxLFxyXG4gICAgS2V5UiA6IDgyLFxyXG4gICAgS2V5UyA6IDgzLFxyXG4gICAgS2V5VCA6IDg0LFxyXG4gICAgS2V5VSA6IDg1LFxyXG4gICAgS2V5ViA6IDg2LFxyXG4gICAgS2V5VyA6IDg3LFxyXG4gICAgS2V5WCA6IDg4LFxyXG4gICAgS2V5WSA6IDg5LFxyXG4gICAgS2V5WiA6IDkwLFxyXG4gICAgS2V5U2hpZnRBIDogOTEsXHJcbiAgICBLZXlTaGlmdEIgOiA5MixcclxuICAgIEtleVNoaWZ0QyA6IDkzLFxyXG4gICAgS2V5U2hpZnREIDogOTQsXHJcbiAgICBLZXlTaGlmdEUgOiA5NSxcclxuICAgIEtleVNoaWZ0RiA6IDk2LFxyXG4gICAgS2V5U2hpZnRHIDogOTcsXHJcbiAgICBLZXlTaGlmdEggOiA5OCxcclxuICAgIEtleVNoaWZ0SSA6IDk5LFxyXG4gICAgS2V5U2hpZnRKIDogMTAwLFxyXG4gICAgS2V5U2hpZnRLIDogMTAxLFxyXG4gICAgS2V5U2hpZnRMIDogMTAyLFxyXG4gICAgS2V5U2hpZnRNIDogMTAzLFxyXG4gICAgS2V5U2hpZnROIDogMTA0LFxyXG4gICAgS2V5U2hpZnRPIDogMTA1LFxyXG4gICAgS2V5U2hpZnRQIDogMTA2LFxyXG4gICAgS2V5U2hpZnRRIDogMTA3LFxyXG4gICAgS2V5U2hpZnRSIDogMTA4LFxyXG4gICAgS2V5U2hpZnRTIDogMTA5LFxyXG4gICAgS2V5U2hpZnRUIDogMTEwLFxyXG4gICAgS2V5U2hpZnRVIDogMTExLFxyXG4gICAgS2V5U2hpZnRWIDogMTEyLFxyXG4gICAgS2V5U2hpZnRXIDogMTEzLFxyXG4gICAgS2V5U2hpZnRYIDogMTE0LFxyXG4gICAgS2V5U2hpZnRZIDogMTE1LFxyXG4gICAgS2V5U2hpZnRaIDogMTE2LFxyXG4gICAgS2V5MCA6IDQ4LFxyXG4gICAgS2V5MSA6IDQ5LFxyXG4gICAgS2V5MiA6IDUwLFxyXG4gICAgS2V5MyA6IDUxLFxyXG4gICAgS2V5NCA6IDUyLFxyXG4gICAgS2V5NSA6IDUzLFxyXG4gICAgS2V5NiA6IDU0LFxyXG4gICAgS2V5NyA6IDU1LFxyXG4gICAgS2V5OCA6IDU2LFxyXG4gICAgS2V5OSA6IDU3LFxyXG4gICAgS2V5QXJyb3dMZWZ0IDogMzcsXHJcbiAgICBLZXlBcnJvd1VwIDogMzgsXHJcbiAgICBLZXlBcnJvd1JpZ2h0IDogMzksXHJcbiAgICBLZXlBcnJvd0Rvd24gOiA0MCxcclxuICAgIEtleUJhY2tzcGFjZSA6IDgsXHJcblxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBLZXlDb2RlczsiXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFyRUE7QUFDQTtBQXVFQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/KeyCodes.js\n");

/***/ }),

/***/ "./src/KeyValues.js":
/*!**************************!*\
  !*** ./src/KeyValues.js ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n    value: true\n});\nvar KeyValues = {\n    32: \" \",\n    65: \"a\",\n    66: \"b\",\n    67: \"c\",\n    68: \"d\",\n    69: \"e\",\n    70: \"f\",\n    71: \"g\",\n    72: \"h\",\n    73: \"i\",\n    74: \"j\",\n    75: \"k\",\n    76: \"l\",\n    77: \"m\",\n    78: \"n\",\n    79: \"o\",\n    80: \"p\",\n    81: \"q\",\n    82: \"r\",\n    83: \"s\",\n    84: \"t\",\n    85: \"u\",\n    86: \"v\",\n    87: \"w\",\n    88: \"x\",\n    89: \"y\",\n    90: \"z\",\n    91: \"A\",\n    92: \"B\",\n    93: \"C\",\n    94: \"D\",\n    95: \"E\",\n    96: \"F\",\n    97: \"G\",\n    98: \"H\",\n    99: \"I\",\n    100: \"J\",\n    101: \"K\",\n    102: \"L\",\n    103: \"M\",\n    104: \"N\",\n    105: \"O\",\n    106: \"P\",\n    107: \"Q\",\n    108: \"R\",\n    109: \"S\",\n    110: \"T\",\n    111: \"U\",\n    112: \"V\",\n    113: \"W\",\n    114: \"X\",\n    115: \"Y\",\n    116: \"Z\",\n    48: \"0\",\n    49: \"1\",\n    50: \"2\",\n    51: \"3\",\n    52: \"4\",\n    53: \"5\",\n    54: \"6\",\n    55: \"7\",\n    56: \"8\",\n    57: \"9\"\n};\n\nexports.default = KeyValues;//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvS2V5VmFsdWVzLmpzLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vL3NyYy9LZXlWYWx1ZXMuanM/NjcyOSJdLCJzb3VyY2VzQ29udGVudCI6WyJ2YXIgS2V5VmFsdWVzID0ge1xyXG4gICAgMzIgOiBcIiBcIixcclxuICAgIDY1IDogXCJhXCIsXHJcbiAgICA2NiA6IFwiYlwiLFxyXG4gICAgNjcgOiBcImNcIixcclxuICAgIDY4IDogXCJkXCIsXHJcbiAgICA2OSA6IFwiZVwiLFxyXG4gICAgNzAgOiBcImZcIixcclxuICAgIDcxIDogXCJnXCIsXHJcbiAgICA3MiA6IFwiaFwiLFxyXG4gICAgNzMgOiBcImlcIixcclxuICAgIDc0IDogXCJqXCIsXHJcbiAgICA3NSA6IFwia1wiLFxyXG4gICAgNzYgOiBcImxcIixcclxuICAgIDc3IDogXCJtXCIsXHJcbiAgICA3OCA6IFwiblwiLFxyXG4gICAgNzkgOiBcIm9cIixcclxuICAgIDgwIDogXCJwXCIsXHJcbiAgICA4MSA6IFwicVwiLFxyXG4gICAgODIgOiBcInJcIixcclxuICAgIDgzIDogXCJzXCIsXHJcbiAgICA4NCA6IFwidFwiLFxyXG4gICAgODUgOiBcInVcIixcclxuICAgIDg2IDogXCJ2XCIsXHJcbiAgICA4NyA6IFwid1wiLFxyXG4gICAgODggOiBcInhcIixcclxuICAgIDg5IDogXCJ5XCIsXHJcbiAgICA5MCA6IFwielwiLFxyXG4gICAgOTEgOiBcIkFcIixcclxuICAgIDkyIDogXCJCXCIsXHJcbiAgICA5MyA6IFwiQ1wiLFxyXG4gICAgOTQgOiBcIkRcIixcclxuICAgIDk1IDogXCJFXCIsXHJcbiAgICA5NiA6IFwiRlwiLFxyXG4gICAgOTcgOiBcIkdcIixcclxuICAgIDk4IDogXCJIXCIsXHJcbiAgICA5OSA6IFwiSVwiLFxyXG4gICAgMTAwIDogXCJKXCIsXHJcbiAgICAxMDEgOiBcIktcIixcclxuICAgIDEwMiA6IFwiTFwiLFxyXG4gICAgMTAzIDogXCJNXCIsXHJcbiAgICAxMDQgOiBcIk5cIixcclxuICAgIDEwNSA6IFwiT1wiLFxyXG4gICAgMTA2IDogXCJQXCIsXHJcbiAgICAxMDcgOiBcIlFcIixcclxuICAgIDEwOCA6IFwiUlwiLFxyXG4gICAgMTA5IDogXCJTXCIsXHJcbiAgICAxMTAgOiBcIlRcIixcclxuICAgIDExMSA6IFwiVVwiLFxyXG4gICAgMTEyIDogXCJWXCIsXHJcbiAgICAxMTMgOiBcIldcIixcclxuICAgIDExNCA6IFwiWFwiLFxyXG4gICAgMTE1IDogXCJZXCIsXHJcbiAgICAxMTYgOiBcIlpcIixcclxuICAgIDQ4IDogXCIwXCIsXHJcbiAgICA0OSA6IFwiMVwiLFxyXG4gICAgNTAgOiBcIjJcIixcclxuICAgIDUxIDogXCIzXCIsXHJcbiAgICA1MiA6IFwiNFwiLFxyXG4gICAgNTMgOiBcIjVcIixcclxuICAgIDU0IDogXCI2XCIsXHJcbiAgICA1NSA6IFwiN1wiLFxyXG4gICAgNTYgOiBcIjhcIixcclxuICAgIDU3IDogXCI5XCIsXHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IEtleVZhbHVlczsiXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUEvREE7QUFDQTtBQWlFQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/KeyValues.js\n");

/***/ }),

/***/ "./src/Main.js":
/*!*********************!*\
  !*** ./src/Main.js ***!
  \*********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n    value: true\n});\n\nvar _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();\n\nvar _assetdirectory = __webpack_require__(/*! ./assetdirectory */ \"./src/assetdirectory.js\");\n\nvar _pixi = __webpack_require__(/*! pixi.js */ \"./node_modules/pixi.js/lib/index.js\");\n\nvar _TitleScreen = __webpack_require__(/*! ./screen/TitleScreen */ \"./src/screen/TitleScreen.js\");\n\nvar _TitleScreen2 = _interopRequireDefault(_TitleScreen);\n\nvar _ = __webpack_require__(/*! . */ \"./src/index.js\");\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nvar Main = function () {\n    function Main() {\n        _classCallCheck(this, Main);\n    }\n\n    _createClass(Main, null, [{\n        key: \"start\",\n        value: function start() {\n            Main.cjAudioQueue = new createjs.LoadQueue();\n            createjs.Sound.alternateExtensions = [\"ogg\"];\n\n            Main.cjAudioQueue.installPlugin(createjs.Sound);\n            Main.cjAudioQueue.addEventListener(\"complete\", Main.handleAudioComplete.bind(Main));\n\n            if (_assetdirectory.AssetDirectory.audio.length > 0) {\n                //LOAD AUDIO                  \n                var audioFiles = _assetdirectory.AssetDirectory.audio;\n                var audioManifest = [];\n                for (var i = 0; i < audioFiles.length; i++) {\n                    audioManifest.push({\n                        id: audioFiles[i],\n                        src: audioFiles[i]\n                    });\n                }\n                Main.cjAudioQueue.loadManifest(audioManifest);\n            } else {\n                Main.handleAudioComplete();\n            }\n        }\n    }, {\n        key: \"handleAudioComplete\",\n        value: function handleAudioComplete() {\n            if (_assetdirectory.AssetDirectory.load.length > 0) {\n                //LOAD IMAGES         \n                var loader = _pixi.loaders.shared;\n                loader.add(_assetdirectory.AssetDirectory.load);\n                loader.load(Main.handleImageComplete);\n            } else {\n                Main.handleImageComplete();\n            }\n        }\n    }, {\n        key: \"handleImageComplete\",\n        value: function handleImageComplete() {\n            var screen = new _TitleScreen2.default();\n            _.App.stage.addChild(screen);\n        }\n    }]);\n\n    return Main;\n}();\n\nexports.default = Main;//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvTWFpbi5qcy5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy9zcmMvTWFpbi5qcz8xMjIxIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEFzc2V0RGlyZWN0b3J5IH0gZnJvbSBcIi4vYXNzZXRkaXJlY3RvcnlcIjtcclxuaW1wb3J0IHsgbG9hZGVycyB9IGZyb20gXCJwaXhpLmpzXCI7XHJcbmltcG9ydCBUaXRsZVNjcmVlbiBmcm9tIFwiLi9zY3JlZW4vVGl0bGVTY3JlZW5cIjtcclxuaW1wb3J0IHsgQXBwIH0gZnJvbSBcIi5cIjtcclxuXHJcblxyXG5jbGFzcyBNYWluIHtcclxuICAgIHN0YXRpYyBzdGFydCgpe1xyXG4gICAgICAgIE1haW4uY2pBdWRpb1F1ZXVlID0gbmV3IGNyZWF0ZWpzLkxvYWRRdWV1ZSgpO1xyXG4gICAgICAgIGNyZWF0ZWpzLlNvdW5kLmFsdGVybmF0ZUV4dGVuc2lvbnMgPSBbXCJvZ2dcIl07XHJcblxyXG4gICAgICAgIE1haW4uY2pBdWRpb1F1ZXVlLmluc3RhbGxQbHVnaW4oY3JlYXRlanMuU291bmQpO1xyXG4gICAgICAgIE1haW4uY2pBdWRpb1F1ZXVlLmFkZEV2ZW50TGlzdGVuZXIoXCJjb21wbGV0ZVwiLCBNYWluLmhhbmRsZUF1ZGlvQ29tcGxldGUuYmluZChNYWluKSk7XHJcblxyXG4gICAgICAgIGlmKEFzc2V0RGlyZWN0b3J5LmF1ZGlvLmxlbmd0aCA+IDApe1xyXG4gICAgICAgICAgICAvL0xPQUQgQVVESU8gICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgbGV0IGF1ZGlvRmlsZXMgPSBBc3NldERpcmVjdG9yeS5hdWRpbztcclxuICAgICAgICAgICAgbGV0IGF1ZGlvTWFuaWZlc3QgPSBbXTtcclxuICAgICAgICAgICAgZm9yKGxldCBpID0gMDsgaSA8IGF1ZGlvRmlsZXMubGVuZ3RoOyBpKyspe1xyXG4gICAgICAgICAgICAgICAgYXVkaW9NYW5pZmVzdC5wdXNoKHtcclxuICAgICAgICAgICAgICAgICAgICBpZDogYXVkaW9GaWxlc1tpXSxcclxuICAgICAgICAgICAgICAgICAgICBzcmM6IGF1ZGlvRmlsZXNbaV1cclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIE1haW4uY2pBdWRpb1F1ZXVlLmxvYWRNYW5pZmVzdChhdWRpb01hbmlmZXN0KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZXtcclxuICAgICAgICAgICAgTWFpbi5oYW5kbGVBdWRpb0NvbXBsZXRlKCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgIH1cclxuXHJcbiAgICBzdGF0aWMgaGFuZGxlQXVkaW9Db21wbGV0ZSgpe1xyXG4gICAgICAgIGlmKEFzc2V0RGlyZWN0b3J5LmxvYWQubGVuZ3RoID4gMCl7XHJcbiAgICAgICAgICAgIC8vTE9BRCBJTUFHRVMgICAgICAgICBcclxuICAgICAgICAgICAgbGV0IGxvYWRlciA9IGxvYWRlcnMuc2hhcmVkO1xyXG4gICAgICAgICAgICBsb2FkZXIuYWRkKEFzc2V0RGlyZWN0b3J5LmxvYWQpO1xyXG4gICAgICAgICAgICBsb2FkZXIubG9hZChNYWluLmhhbmRsZUltYWdlQ29tcGxldGUpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBlbHNlIHtcclxuICAgICAgICAgICAgTWFpbi5oYW5kbGVJbWFnZUNvbXBsZXRlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBoYW5kbGVJbWFnZUNvbXBsZXRlKCl7XHJcbiAgICAgICAgbGV0IHNjcmVlbiA9IG5ldyBUaXRsZVNjcmVlbigpO1xyXG4gICAgICAgIEFwcC5zdGFnZS5hZGRDaGlsZChzY3JlZW4pO1xyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBNYWluOyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7OztBQUFBO0FBQ0E7Ozs7O0FBRUE7Ozs7Ozs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFFQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFHQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/Main.js\n");

/***/ }),

/***/ "./src/assetdirectory.js":
/*!*******************************!*\
  !*** ./src/assetdirectory.js ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n\tvalue: true\n});\nvar AssetDirectory = exports.AssetDirectory = {\n\t\"load\": [\"assets/images/image1.jpg\", \"assets/images/image2.jpg\", \"assets/images/image3.jpg\"],\n\t\"audio\": [\"assets/audio/Layer Cake.mp3\", \"assets/audio/Persona 5 Notification Tone.mp3\", \"assets/audio/Persona 5 Stat Up.mp3\", \"assets/audio/Persona Summon Sound Effect.mp3\", \"assets/audio/Price.mp3\"]\n};//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXNzZXRkaXJlY3RvcnkuanMuanMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vc3JjL2Fzc2V0ZGlyZWN0b3J5LmpzPzZmZDEiXSwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGxldCBBc3NldERpcmVjdG9yeSA9IHtcblx0XCJsb2FkXCI6IFtcblx0XHRcImFzc2V0cy9pbWFnZXMvaW1hZ2UxLmpwZ1wiLFxuXHRcdFwiYXNzZXRzL2ltYWdlcy9pbWFnZTIuanBnXCIsXG5cdFx0XCJhc3NldHMvaW1hZ2VzL2ltYWdlMy5qcGdcIlxuXHRdLFxuXHRcImF1ZGlvXCI6IFtcblx0XHRcImFzc2V0cy9hdWRpby9MYXllciBDYWtlLm1wM1wiLFxuXHRcdFwiYXNzZXRzL2F1ZGlvL1BlcnNvbmEgNSBOb3RpZmljYXRpb24gVG9uZS5tcDNcIixcblx0XHRcImFzc2V0cy9hdWRpby9QZXJzb25hIDUgU3RhdCBVcC5tcDNcIixcblx0XHRcImFzc2V0cy9hdWRpby9QZXJzb25hIFN1bW1vbiBTb3VuZCBFZmZlY3QubXAzXCIsXG5cdFx0XCJhc3NldHMvYXVkaW8vUHJpY2UubXAzXCJcblx0XVxufTsiXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUE7QUFDQTtBQUtBO0FBTkEiLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./src/assetdirectory.js\n");

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n    value: true\n});\nexports.App = undefined;\n\nvar _assetdirectory = __webpack_require__(/*! ./assetdirectory.js */ \"./src/assetdirectory.js\");\n\nvar _pixi = __webpack_require__(/*! pixi.js */ \"./node_modules/pixi.js/lib/index.js\");\n\nvar _Main = __webpack_require__(/*! ./Main.js */ \"./src/Main.js\");\n\nvar _Main2 = _interopRequireDefault(_Main);\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nvar Config = __webpack_require__(/*! Config */ \"Config\");\n\nvar canvas = document.getElementById('game-canvas');\nvar pixiapp = new _pixi.Application({\n    view: canvas,\n    width: Config.BUILD.WIDTH,\n    height: Config.BUILD.HEIGHT\n});\n\ndocument.body.style.margin = \"0px\";\ndocument.body.style.overflow = \"hidden\";\n\n/*****************************************\r\n ************* ENTRY POINT ***************\r\n *****************************************/\nfunction ready(fn) {\n    if (document.readyState != 'loading') {\n        fn();\n    } else {\n        document.addEventListener('DOMContentLoaded', fn);\n    }\n}\n\nready(function () {\n    _Main2.default.start();\n});\n\nexports.App = pixiapp;//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvaW5kZXguanMuanMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vc3JjL2luZGV4LmpzPzEyZDUiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQXNzZXREaXJlY3RvcnkgfSBmcm9tICcuL2Fzc2V0ZGlyZWN0b3J5LmpzJztcclxuaW1wb3J0IHsgQXBwbGljYXRpb24gfSBmcm9tICdwaXhpLmpzJztcclxuaW1wb3J0IE1haW4gZnJvbSAnLi9NYWluLmpzJztcclxuXHJcbnZhciBDb25maWcgPSByZXF1aXJlKCdDb25maWcnKTtcclxuXHJcblxyXG5sZXQgY2FudmFzID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2dhbWUtY2FudmFzJyk7XHJcbmxldCBwaXhpYXBwID0gbmV3IEFwcGxpY2F0aW9uKHtcclxuICAgIHZpZXc6IGNhbnZhcyxcclxuICAgIHdpZHRoOiBDb25maWcuQlVJTEQuV0lEVEgsXHJcbiAgICBoZWlnaHQ6IENvbmZpZy5CVUlMRC5IRUlHSFRcclxufSlcclxuXHJcblxyXG5kb2N1bWVudC5ib2R5LnN0eWxlLm1hcmdpbiA9IFwiMHB4XCI7XHJcbmRvY3VtZW50LmJvZHkuc3R5bGUub3ZlcmZsb3cgPSBcImhpZGRlblwiO1xyXG5cclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXHJcbiAqKioqKioqKioqKioqIEVOVFJZIFBPSU5UICoqKioqKioqKioqKioqKlxyXG4gKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcbmZ1bmN0aW9uIHJlYWR5KGZuKSB7XHJcbiAgICBpZiAoZG9jdW1lbnQucmVhZHlTdGF0ZSAhPSAnbG9hZGluZycpIHtcclxuICAgICAgICBmbigpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgICBkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCdET01Db250ZW50TG9hZGVkJywgZm4pO1xyXG4gICAgfVxyXG59XHJcblxyXG5yZWFkeShmdW5jdGlvbiAoKSB7XHJcbiAgICBNYWluLnN0YXJ0KCk7XHJcbn0pO1xyXG5cclxuZXhwb3J0IHtwaXhpYXBwIGFzIEFwcH07Il0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBOzs7OztBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFDQTtBQU1BO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/index.js\n");

/***/ }),

/***/ "./src/screen/TitleScreen.js":
/*!***********************************!*\
  !*** ./src/screen/TitleScreen.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n    value: true\n});\n\nvar _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();\n\nvar _pixi = __webpack_require__(/*! pixi.js */ \"./node_modules/pixi.js/lib/index.js\");\n\nvar _domain = __webpack_require__(/*! domain */ \"./node_modules/domain-browser/source/index.js\");\n\nvar _KeyCodes = __webpack_require__(/*! ../KeyCodes */ \"./src/KeyCodes.js\");\n\nvar _KeyCodes2 = _interopRequireDefault(_KeyCodes);\n\nvar _KeyValues = __webpack_require__(/*! ../KeyValues */ \"./src/KeyValues.js\");\n\nvar _KeyValues2 = _interopRequireDefault(_KeyValues);\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nfunction _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError(\"this hasn't been initialised - super() hasn't been called\"); } return call && (typeof call === \"object\" || typeof call === \"function\") ? call : self; }\n\nfunction _inherits(subClass, superClass) { if (typeof superClass !== \"function\" && superClass !== null) { throw new TypeError(\"Super expression must either be null or a function, not \" + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }\n\nvar sprite = [];\n\nvar TitleScreen = function (_Container) {\n    _inherits(TitleScreen, _Container);\n\n    function TitleScreen() {\n        _classCallCheck(this, TitleScreen);\n\n        var _this = _possibleConstructorReturn(this, (TitleScreen.__proto__ || Object.getPrototypeOf(TitleScreen)).call(this));\n\n        _this.interactive = true;\n        _this.elapsedTime = 0;\n        _this.ticker = _pixi.ticker.shared;\n        _this.ticker.add(_this.update, _this);\n\n        _this.rect = new _pixi.Graphics();\n        _this.rect.beginFill(0x000000);\n        _this.rect.drawRect(0, 0, 900, 600);\n        _this.rect.endFill();\n        _this.addChild(_this.rect);\n\n        _this.rect1 = new _pixi.Graphics();\n        _this.rect1.beginFill(0x00FF00);\n        _this.rect1.drawRect(400, 250, 15, 20);\n        _this.rect1.endFill();\n        _this.addChild(_this.rect1);\n\n        _this.rect3 = new _pixi.Graphics();\n        _this.rect3.beginFill(0x0000FF);\n        _this.rect3.drawRect(50, 50, 10, 25);\n        _this.rect3.endFill();\n        _this.addChild(_this.rect3);\n\n        _this.rect4 = new _pixi.Graphics();\n        _this.rect4.beginFill(0x0000FF);\n        _this.rect4.drawRect(100, 100, 10, 25);\n        _this.rect4.endFill();\n        _this.rect4.pivot.set(100, 100);\n        _this.addChild(_this.rect4);\n\n        return _this;\n    }\n\n    _createClass(TitleScreen, [{\n        key: \"mousedown\",\n        value: function mousedown(e) {\n            if (this.rect.getBounds().contains(e.data.global.x, e.data.global.y)) {\n                this.rect2 = new _pixi.Graphics();\n                this.rect2.beginFill(0xFF0000);\n                this.rect2.drawRect(e.data.global.x, e.data.global.y, 15, 20);\n                this.rect2.endFill();\n                this.addChild(this.rect2);\n            }\n        }\n    }, {\n        key: \"mousemove\",\n        value: function mousemove(e) {\n            if (this.rect.getBounds().contains(e.data.global.x, e.data.global.y)) {\n                this.rect1.rotation = e.data.global.xy;\n            }\n        }\n    }, {\n        key: \"update\",\n        value: function update(st) {\n            var _this2 = this;\n\n            this.elapsedTime += this.ticker.elapsedMS;\n            this.rect3.x += this.ticker.elapsedMS * 0.1;\n            this.rect4.rotation += 0.1;\n\n            setInterval(function () {\n                _this2.rect3 = new _pixi.Graphics();\n                _this2.rect3.beginFill(0x0000FF);\n                _this2.rect3.drawRect(50, 50, 10, 25);\n                _this2.rect3.endFill();\n                sprite.push(_this2.rect3);\n                _this2.addChild(_this2.rect3);\n            }, 1000);\n\n            // if(this.elapsedTime > 1000)\n            // {\n            //     this.ticker.remove(this.update, this);\n            // }\n        }\n    }]);\n\n    return TitleScreen;\n}(_pixi.Container);\n\nexports.default = TitleScreen;//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvc2NyZWVuL1RpdGxlU2NyZWVuLmpzLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vL3NyYy9zY3JlZW4vVGl0bGVTY3JlZW4uanM/YTM3MiJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb250YWluZXIgLCBHcmFwaGljcywgVGV4dCwgdGlja2VyLCB1dGlscyB9IGZyb20gXCJwaXhpLmpzXCI7XHJcbmltcG9ydCB7IGNyZWF0ZSB9IGZyb20gXCJkb21haW5cIjtcclxuaW1wb3J0IEtleUNvZGVzIGZyb20gXCIuLi9LZXlDb2Rlc1wiO1xyXG5pbXBvcnQgS2V5VmFsdWVzIGZyb20gXCIuLi9LZXlWYWx1ZXNcIjtcclxuXHJcblxyXG52YXIgc3ByaXRlID0gW107XHJcbmNsYXNzIFRpdGxlU2NyZWVuIGV4dGVuZHMgQ29udGFpbmVye1xyXG4gICAgY29uc3RydWN0b3IoKXtcclxuICAgICAgICBzdXBlcigpO1xyXG5cclxuICAgICAgICB0aGlzLmludGVyYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLmVsYXBzZWRUaW1lID0gMDtcclxuICAgICAgICB0aGlzLnRpY2tlciA9IHRpY2tlci5zaGFyZWQ7XHJcbiAgICAgICAgdGhpcy50aWNrZXIuYWRkKHRoaXMudXBkYXRlLCB0aGlzKTtcclxuXHJcbiAgICAgICAgdGhpcy5yZWN0ID0gbmV3IEdyYXBoaWNzKCk7XHJcbiAgICAgICAgdGhpcy5yZWN0LmJlZ2luRmlsbCgweDAwMDAwMCk7XHJcbiAgICAgICAgdGhpcy5yZWN0LmRyYXdSZWN0KDAsMCw5MDAsNjAwKTtcclxuICAgICAgICB0aGlzLnJlY3QuZW5kRmlsbCgpO1xyXG4gICAgICAgIHRoaXMuYWRkQ2hpbGQodGhpcy5yZWN0KTtcclxuXHJcbiAgICAgICAgdGhpcy5yZWN0MSA9IG5ldyBHcmFwaGljcygpO1xyXG4gICAgICAgIHRoaXMucmVjdDEuYmVnaW5GaWxsKDB4MDBGRjAwKTtcclxuICAgICAgICB0aGlzLnJlY3QxLmRyYXdSZWN0KDQwMCwyNTAsMTUsMjApO1xyXG4gICAgICAgIHRoaXMucmVjdDEuZW5kRmlsbCgpO1xyXG4gICAgICAgIHRoaXMuYWRkQ2hpbGQodGhpcy5yZWN0MSk7XHJcblxyXG4gICAgICAgIHRoaXMucmVjdDMgPSBuZXcgR3JhcGhpY3MoKTtcclxuICAgICAgICB0aGlzLnJlY3QzLmJlZ2luRmlsbCgweDAwMDBGRik7XHJcbiAgICAgICAgdGhpcy5yZWN0My5kcmF3UmVjdCg1MCw1MCwxMCwyNSk7XHJcbiAgICAgICAgdGhpcy5yZWN0My5lbmRGaWxsKCk7XHJcbiAgICAgICAgdGhpcy5hZGRDaGlsZCh0aGlzLnJlY3QzKTtcclxuXHJcbiAgICAgICAgdGhpcy5yZWN0NCA9IG5ldyBHcmFwaGljcygpO1xyXG4gICAgICAgIHRoaXMucmVjdDQuYmVnaW5GaWxsKDB4MDAwMEZGKTtcclxuICAgICAgICB0aGlzLnJlY3Q0LmRyYXdSZWN0KDEwMCwxMDAsMTAsMjUpO1xyXG4gICAgICAgIHRoaXMucmVjdDQuZW5kRmlsbCgpO1xyXG4gICAgICAgIHRoaXMucmVjdDQucGl2b3Quc2V0KDEwMCwxMDApO1xyXG4gICAgICAgIHRoaXMuYWRkQ2hpbGQodGhpcy5yZWN0NCk7XHJcbiAgICAgICAgXHJcblxyXG4gICAgICAgIFxyXG4gICAgfVxyXG5cclxuICAgIG1vdXNlZG93bihlKVxyXG4gICAge1xyXG4gICAgICAgIGlmKHRoaXMucmVjdC5nZXRCb3VuZHMoKS5jb250YWlucyhlLmRhdGEuZ2xvYmFsLngsZS5kYXRhLmdsb2JhbC55KSlcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIHRoaXMucmVjdDIgPSBuZXcgR3JhcGhpY3MoKTtcclxuICAgICAgICAgICAgdGhpcy5yZWN0Mi5iZWdpbkZpbGwoMHhGRjAwMDApO1xyXG4gICAgICAgICAgICB0aGlzLnJlY3QyLmRyYXdSZWN0KGUuZGF0YS5nbG9iYWwueCxlLmRhdGEuZ2xvYmFsLnksMTUsMjApO1xyXG4gICAgICAgICAgICB0aGlzLnJlY3QyLmVuZEZpbGwoKTtcclxuICAgICAgICAgICAgdGhpcy5hZGRDaGlsZCh0aGlzLnJlY3QyKTtcclxuXHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG1vdXNlbW92ZShlKVxyXG4gICAge1xyXG4gICAgICAgIGlmKHRoaXMucmVjdC5nZXRCb3VuZHMoKS5jb250YWlucyhlLmRhdGEuZ2xvYmFsLngsZS5kYXRhLmdsb2JhbC55KSlcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIHRoaXMucmVjdDEucm90YXRpb24gPSBlLmRhdGEuZ2xvYmFsLnh5O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICB1cGRhdGUoc3QpXHJcbiAgICB7XHJcbiAgICAgICAgdGhpcy5lbGFwc2VkVGltZSArPSB0aGlzLnRpY2tlci5lbGFwc2VkTVM7XHJcbiAgICAgICAgdGhpcy5yZWN0My54ICs9IHRoaXMudGlja2VyLmVsYXBzZWRNUyAqIDAuMTtcclxuICAgICAgICB0aGlzLnJlY3Q0LnJvdGF0aW9uICs9IDAuMTtcclxuICAgICAgICBcclxuICAgICAgICBzZXRJbnRlcnZhbCggKCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLnJlY3QzID0gbmV3IEdyYXBoaWNzKCk7XHJcbiAgICAgICAgICAgIHRoaXMucmVjdDMuYmVnaW5GaWxsKDB4MDAwMEZGKTtcclxuICAgICAgICAgICAgdGhpcy5yZWN0My5kcmF3UmVjdCg1MCw1MCwxMCwyNSk7XHJcbiAgICAgICAgICAgIHRoaXMucmVjdDMuZW5kRmlsbCgpO1xyXG4gICAgICAgICAgICBzcHJpdGUucHVzaCh0aGlzLnJlY3QzKVxyXG4gICAgICAgICAgICB0aGlzLmFkZENoaWxkKHRoaXMucmVjdDMpO1xyXG4gICAgICAgIH0sIDEwMDApO1xyXG5cclxuICAgICAgICAvLyBpZih0aGlzLmVsYXBzZWRUaW1lID4gMTAwMClcclxuICAgICAgICAvLyB7XHJcbiAgICAgICAgLy8gICAgIHRoaXMudGlja2VyLnJlbW92ZSh0aGlzLnVwZGF0ZSwgdGhpcyk7XHJcbiAgICAgICAgLy8gfVxyXG4gICAgfVxyXG4gICBcclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgVGl0bGVTY3JlZW47Il0sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTs7O0FBQUE7QUFDQTs7Ozs7Ozs7Ozs7QUFFQTtBQUNBO0FBQUE7OztBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFoQ0E7QUFtQ0E7QUFDQTs7O0FBQ0E7QUFFQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBOzs7QUFFQTtBQUVBO0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUE5RUE7QUFDQTtBQWlGQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/screen/TitleScreen.js\n");

/***/ }),

/***/ "Config":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** external "{\"BUILD\":{\"GAME_TITLE\":\"GAME TITLE\",\"VERSION\":\"0.0.1\",\"WIDTH\":889,\"HEIGHT\":500,\"MINIFIED_EXTERNAL_JS\":false,\"MINIFIED_EXTERNAL_JS_PATH\":\"./src/external/minified\",\"UNMINIFIED_EXTERNAL_JS_PATH\":\"./src/external/unminified\",\"EXTERNAL_JS\":[\"soundjs.js\",\"preloadjs.js\"],\"ASSETPATH\":{\"load\":\"assets\",\"audio\":\"assets/audio\"}}}" ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = {\"BUILD\":{\"GAME_TITLE\":\"GAME TITLE\",\"VERSION\":\"0.0.1\",\"WIDTH\":889,\"HEIGHT\":500,\"MINIFIED_EXTERNAL_JS\":false,\"MINIFIED_EXTERNAL_JS_PATH\":\"./src/external/minified\",\"UNMINIFIED_EXTERNAL_JS_PATH\":\"./src/external/unminified\",\"EXTERNAL_JS\":[\"soundjs.js\",\"preloadjs.js\"],\"ASSETPATH\":{\"load\":\"assets\",\"audio\":\"assets/audio\"}}};//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ29uZmlnLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwie1xcXCJCVUlMRFxcXCI6e1xcXCJHQU1FX1RJVExFXFxcIjpcXFwiR0FNRSBUSVRMRVxcXCIsXFxcIlZFUlNJT05cXFwiOlxcXCIwLjAuMVxcXCIsXFxcIldJRFRIXFxcIjo4ODksXFxcIkhFSUdIVFxcXCI6NTAwLFxcXCJNSU5JRklFRF9FWFRFUk5BTF9KU1xcXCI6ZmFsc2UsXFxcIk1JTklGSUVEX0VYVEVSTkFMX0pTX1BBVEhcXFwiOlxcXCIuL3NyYy9leHRlcm5hbC9taW5pZmllZFxcXCIsXFxcIlVOTUlOSUZJRURfRVhURVJOQUxfSlNfUEFUSFxcXCI6XFxcIi4vc3JjL2V4dGVybmFsL3VubWluaWZpZWRcXFwiLFxcXCJFWFRFUk5BTF9KU1xcXCI6W1xcXCJzb3VuZGpzLmpzXFxcIixcXFwicHJlbG9hZGpzLmpzXFxcIl0sXFxcIkFTU0VUUEFUSFxcXCI6e1xcXCJsb2FkXFxcIjpcXFwiYXNzZXRzXFxcIixcXFwiYXVkaW9cXFwiOlxcXCJhc3NldHMvYXVkaW9cXFwifX19XCI/ZDE0YiJdLCJzb3VyY2VzQ29udGVudCI6WyJtb2R1bGUuZXhwb3J0cyA9IHtcIkJVSUxEXCI6e1wiR0FNRV9USVRMRVwiOlwiR0FNRSBUSVRMRVwiLFwiVkVSU0lPTlwiOlwiMC4wLjFcIixcIldJRFRIXCI6ODg5LFwiSEVJR0hUXCI6NTAwLFwiTUlOSUZJRURfRVhURVJOQUxfSlNcIjpmYWxzZSxcIk1JTklGSUVEX0VYVEVSTkFMX0pTX1BBVEhcIjpcIi4vc3JjL2V4dGVybmFsL21pbmlmaWVkXCIsXCJVTk1JTklGSUVEX0VYVEVSTkFMX0pTX1BBVEhcIjpcIi4vc3JjL2V4dGVybmFsL3VubWluaWZpZWRcIixcIkVYVEVSTkFMX0pTXCI6W1wic291bmRqcy5qc1wiLFwicHJlbG9hZGpzLmpzXCJdLFwiQVNTRVRQQVRIXCI6e1wibG9hZFwiOlwiYXNzZXRzXCIsXCJhdWRpb1wiOlwiYXNzZXRzL2F1ZGlvXCJ9fX07Il0sIm1hcHBpbmdzIjoiQUFBQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///Config\n");

/***/ })

/******/ });