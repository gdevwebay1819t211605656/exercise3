import { Container , Graphics, Text, ticker, utils } from "pixi.js";
import { create } from "domain";
import KeyCodes from "../KeyCodes";
import KeyValues from "../KeyValues";



class TitleScreen extends Container{
    constructor(){
        super();

        this.interactive = true;
        this.elapsedTime = 0;
        this.ticker = ticker.shared;
        this.ticker.add(this.update, this);
        this.sprite = [];

        this.rect = new Graphics();
        this.rect.beginFill(0x000000);
        this.rect.drawRect(0,0,900,600);
        this.rect.endFill();
        this.addChild(this.rect);

        this.rect1 = new Graphics();
        this.rect1.beginFill(0x00FF00);
        this.rect1.drawRect(400,250,15,20);
        this.rect1.endFill();
        this.addChild(this.rect1);

        this.rect3 = new Graphics();
        this.rect3.beginFill(0x0000FF);
        this.rect3.drawRect(50,50,10,25);
        this.rect3.endFill();
        this.addChild(this.rect3);

        this.rect4 = new Graphics();
        this.rect4.beginFill(0x0000FF);
        this.rect4.drawRect(100,100,10,25);
        this.rect4.endFill();
        this.rect4.pivot.set(100,100);
        this.addChild(this.rect4);
        
        setInterval( () => {
            this.rect3 = new Graphics();
            this.rect3.beginFill(0x0000FF);
            this.rect3.drawRect(50,50,10,25);
            this.rect3.endFill();
            this.sprite.push(this.rect3)
            this.addChild(this.rect3);
        }, 1000);
        
    }

    mousedown(e)
    {
        if(this.rect.getBounds().contains(e.data.global.x,e.data.global.y))
        {
            this.rect2 = new Graphics();
            this.rect2.beginFill(0xFF0000);
            this.rect2.drawRect(e.data.global.x,e.data.global.y,15,20);
            this.rect2.endFill();
            this.addChild(this.rect2);

        }
    }

    mousemove(e)
    {
        if(this.rect.getBounds().contains(e.data.global.x,e.data.global.y))
        {
            this.rect1.rotation = e.data.global.xy;
        }
    }

    update(st)
    {
        this.elapsedTime += this.ticker.elapsedMS;
        
        
        for(var e = this.sprite.length - 1; e>=0;e--)
        {
            
            this.sprite[e].x -= this.ticker.elapsedMS * 0.1;
            
        }
    }
   
}

export default TitleScreen;